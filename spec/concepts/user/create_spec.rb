require 'rails_helper'

RSpec.describe User::Create do

  context 'api call from user' do
    let(:params_success) { { params: { id: 13458, email: 'jojoqa@mailinator.com', company_id: 414, user_company_id: 2353 } } }
    subject { described_class.(params_success) }

    it 'success' do
      matchers = params_success[:params]

      expect{ subject }.to_not raise_error()
      expect( subject[:model].id ).to               eq(matchers[:id])
      expect( subject[:model].email ).to            eq(matchers[:email])
      expect( subject[:model].company_id ).to       eq(matchers[:company_id])
      expect( subject[:model].user_company_id ).to  eq(matchers[:user_company_id])
      expect( subject["contract.default"].errors.messages.keys.length ).to eq(0)
    end
  end

  context 'negative case' do
    subject { described_class.({}) }

    it 'error' do
      expect { subject }.to raise_error(ArgumentError)
      expect { subject }.to raise_error('missing keyword: params')
      expect { subject }.to raise_error(ArgumentError, 'missing keyword: params')
      expect { subject }.to raise_error(an_instance_of(ArgumentError).and having_attributes(message: 'missing keyword: params'))
    end
  end

  context 'empty params' do
    subject { described_class.( params: {}) }

    it 'empty argument' do
      expect { subject }.to_not raise_error()
      expect( subject["contract.default"].errors.messages.keys.length ).to eq(4)
    end

  end
end
