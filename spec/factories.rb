FactoryBot.define do

  factory :user, class: 'User' do
    id { 13458 }
    email { 'jojoqa@mailinator.com' }
    company_id { 414 }
    user_company_id { 2353 }
  end

  factory :ore, class: 'Ore' do
    name { 'Ruby' }
    description { 'A ruby is a pink to blood-red colored gemstone, a variety of the mineral corundum. Other varieties of gem-quality corundum are called sapphires. Ruby is one of the traditional cardinal gems, together with amethyst, sapphire, emerald, and diamond. The word ruby comes from ruber, Latin for red.' }
    price { 100 }
    discovered_at { 3.minutes.ago }
    created_by { 1.minutes.ago }
    updated_by { 2353 }
    deleted_at { nil }
    deleted_by { nil }
  end

end
