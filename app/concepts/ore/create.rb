class Ore::Create < Trailblazer::Operation
  include OresHelper

  step Model( Ore, :new )
  step Contract::Build( constant: Ore::Contract::Create )
  step Contract::Validate()
  step Contract::Persist( method: :sync )
  step :append_stamps!
  pass :persist!

  def append_stamps!(options, model:, user:, **)
    model.created_by = model.updated_by = user.user_company_id
  end

end
