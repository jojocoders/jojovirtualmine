module Ore::Contract
  class Index < Reform::Form
    property :price_min
    property :price_max

    validates :price_min, numericality: true, presence: false, allow_nil: true
    validates :price_max, numericality: true, presence: false, allow_nil: true
  end
end
