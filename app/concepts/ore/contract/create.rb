module Ore::Contract
  class Create < Reform::Form
    property :name
    property :description
    property :price
    property :discovered_at

    validates :name, length: 2..33, presence: true # https://guides.rubyonrails.org/active_record_validations.html#length
    validates :price, numericality: true, presence: true
  end
end
