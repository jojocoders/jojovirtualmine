class Ore::Update < Trailblazer::Operation
  include OresHelper

  step Model( Ore, :find_by )
  step :filter!
  step Contract::Build( constant: Ore::Contract::Create )
  step Contract::Validate()
  step Contract::Persist( method: :sync )
  step :append_stamps!
  pass :persist!

  def filter!(options, model:, **)
    return false if model.deleted_at.present?
    return true
  end

  def append_stamps!(options, model:, user:, **)
    model.updated_by = user.user_company_id
  end

end
