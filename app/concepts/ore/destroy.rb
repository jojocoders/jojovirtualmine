class Ore::Destroy < Trailblazer::Operation
  include OresHelper

  step Model( Ore, :find )
  step :filter!
  step :append_stamps!
  pass :persist!

  def filter!(options, model:, **)
    return options[:model] = nil if model.deleted_at.present?
    true
  end

  def append_stamps!(options, model:, user:, **)
    model.deleted_by = model.updated_by = user.user_company_id
    model.deleted_at = DateTime.now
  end

end
