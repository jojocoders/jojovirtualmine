class Ore::Show < Trailblazer::Operation
  step Model( Ore, :find )
  pass :filter!

  def filter!(options, model:, **)
    options[:model] = nil if model.deleted_at.present?
  end
end
