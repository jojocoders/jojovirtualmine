class Ore::Index < Trailblazer::Operation
  include Constant::Query

  class Params
    attr_accessor :price_min, :price_max
  end

  step Model( Params, :new )
  step Contract::Build( constant: Ore::Contract::Index )
  step Contract::Validate()
  step Contract::Persist( method: :sync )
  step :initialize!
  pass :build_query!
  pass :append_default_query!
  pass :execute_query!

  def initialize!(options, model:, **)
    options[:query] = {}
    options[:query_keys] = model.instance_values.compact.keys
  end

  def build_query!(options, model:, query:, query_keys:, **)
    query = set_price_query(model, query) if (query_keys & PRICE_QUERY_KEYS).any?
  end

  def append_default_query!(options, model:, query:, query_keys:, **)
    query[:deleted_at] = nil
  end

  def execute_query!(options, query:, **)
    options[:model] = Ore.where(query)
  end

  private

  def set_price_query(model, query)
    params = model.instance_values
    price_min = params['price_min'] ? params['price_min'].to_i : -Float::INFINITY
    price_max = params['price_max'] ? params['price_max'].to_i :  Float::INFINITY
    query[:price] = price_min..price_max
  end
end
