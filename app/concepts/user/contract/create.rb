module User::Contract
  class Create < Reform::Form
    property :id
    property :email
    property :company_id
    property :user_company_id

    validates :id,              presence: true, numericality: true
    validates :email,           presence: true
    validates :company_id,      presence: true, numericality: true
    validates :user_company_id, presence: true, numericality: true
  end
end
