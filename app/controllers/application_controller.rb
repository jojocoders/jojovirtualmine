class ApplicationController < ActionController::Base
  skip_before_action :verify_authenticity_token

  before_action do
    authorize
    parse_params
  end

  def authorize
    raise NoMethodError.new('declare authorize in your controller class')
  end

  protected

  def parse_params
    if request.headers['Content-Type'] == 'application/json'
      data = JSON.parse(request.body.read) rescue {}
      params.merge! data
    end
  end

  def user
    decoded_token = decode_token
    User::Create.( params: decoded_token[:user] )[:model]
  end

  def decode_token
    token   = request.env['HTTP_AUTHORIZATION']
    JWT.decode(token, ENV['JWT_SECRET_KEY'], true, { algorithm: 'HS256' }).first.deep_symbolize_keys! rescue false
  end

  def user_authorize
    #not best practice
    raise '401' unless user
  end

  def generate_generic_response(result)
    if result.success?
      @data, @status = [result[:model], 'success']
    else
      @data = result['result.contract.default'].errors.messages rescue nil
      @status = 'fail'
    end
  end
end
