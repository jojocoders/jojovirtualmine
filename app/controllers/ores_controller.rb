class OresController < ApplicationController
  # https://github.com/omniti-labs/jsend
  def create
    result  = Ore::Create.( params: params, user: user )
    generate_generic_response(result)
  end

  def destroy
    result  = Ore::Destroy.( params: params, user: user )
    @data, @status = [result[:model], 'success'] if result.success?
  end

  # https://blog.mwaysolutions.com/2014/06/05/10-best-practices-for-better-restful-api/
  # https://hackernoon.com/restful-api-designing-guidelines-the-best-practices-60e1d954e7c9
  # https://restfulapi.net/resource-naming/
  # https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design
  def index
    result  = Ore::Index.( params: params, user: user )
    generate_generic_response(result)
  end

  def show
    result  = Ore::Show.( params: params )
    @data, @status = [result[:model], 'success'] if result.success?
  end

  def update
    result  = Ore::Update.( params: params, user: user )
    generate_generic_response(result)
  end

  private

  def authorize
    user_authorize
  end

end
