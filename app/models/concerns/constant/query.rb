module Constant
  module Query

    PRICE_QUERY_KEYS = [
      'price_min',
      'price_max'
    ].freeze

  end
end
