# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_12_035643) do

  create_table "daily_productions", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "ore_id"
    t.text "notes"
    t.integer "quantity"
    t.integer "supervised_by"
    t.date "cutoff_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ore_id", "cutoff_date"], name: "index_daily_productions_on_ore_id_and_cutoff_date", unique: true
    t.index ["ore_id"], name: "index_daily_productions_on_ore_id"
  end

  create_table "ores", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "price"
    t.datetime "discovered_at"
    t.integer "created_by"
    t.integer "updated_by"
    t.datetime "deleted_at"
    t.integer "deleted_by"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
