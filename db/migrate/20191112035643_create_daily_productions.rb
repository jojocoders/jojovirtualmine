class CreateDailyProductions < ActiveRecord::Migration[6.0]
  def change
    create_table :daily_productions do |t|
      t.belongs_to  :ore
      t.text        :notes
      t.integer     :quantity
      t.integer     :supervised_by
      t.date        :cutoff_date

      t.timestamps
      t.index [:ore_id, :cutoff_date], unique: true
    end
  end
end
