class CreateOres < ActiveRecord::Migration[6.0]
  def change
    create_table :ores do |t| # https://edgeapi.rubyonrails.org/classes/ActiveRecord/ConnectionAdapters/SchemaStatements.html#method-i-add_column
      t.string    :name
      t.text      :description
      t.integer   :price
      t.datetime  :discovered_at
      t.integer   :created_by
      t.integer   :updated_by
      t.datetime  :deleted_at
      t.integer   :deleted_by

      t.timestamps
    end
  end
end
