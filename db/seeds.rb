# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Ore
Ore.create!([
  {
    name:           'Ruby',
    description:    'A ruby is a pink to blood-red colored gemstone, a variety of the mineral corundum. Other varieties of gem-quality corundum are called sapphires. Ruby is one of the traditional cardinal gems, together with amethyst, sapphire, emerald, and diamond. The word ruby comes from ruber, Latin for red.',
    price:          50000,
    discovered_at:  DateTime.now.yesterday.beginning_of_day,
    deleted_at:     DateTime.now
  }, {
    name:           'Diamond',
    description:    'Diamond is a solid form of the element carbon with its atoms arranged in a crystal structure called diamond cubic. At room temperature and pressure, another solid form of carbon known as graphite is the chemically stable form, but diamond almost never converts to it.',
    price:          50000,
    discovered_at:  5.days.ago
  }
])

Ore.find_by( name: 'Ruby' ).daily_production.create!([
  {
    notes:          'No problem occured.',
    quantity:       50000,
    supervised_by:  2353,
    cutoff_date:    Date.today
  },
  {
    notes:          'No problem occured.',
    quantity:       40000,
    supervised_by:  2353,
    cutoff_date:    Date.yesterday
  },
  {
    notes:          'No problem occured.',
    quantity:       30000,
    supervised_by:  2353,
    cutoff_date:    Date.yesterday.yesterday
  },
  {
    notes:          'Drill head was broken',
    quantity:       20000,
    supervised_by:  2353,
    cutoff_date:    Date.yesterday.yesterday.yesterday
  }
])

Ore.find_by( name: 'Diamond' ).daily_production.create!([
  {
    notes:          'No problem occured',
    quantity:       20000,
    supervised_by:  2353,
    cutoff_date:    0.day.ago.to_date
  },
  {
    notes:          'No problem occured',
    quantity:       30000,
    supervised_by:  2353,
    cutoff_date:    1.day.ago.to_date
  },
  {
    notes:          'No problem occured',
    quantity:       40000,
    supervised_by:  2353,
    cutoff_date:    2.day.ago.to_date
  },
  {
    notes:          'No problem occured',
    quantity:       50000,
    supervised_by:  2353,
    cutoff_date:    3.day.ago.to_date
  }
])
