# jojovirtualmine

This mine contain lots of rubies.

# Dependencies
MySQL  
Ruby v2.6.5  
Rails v6.0.0

# Setup
1. Setup git and RVM, you may check it [here](https://jojonomic.atlassian.net/wiki/spaces/BAC/pages/105119767)
2. Clone repo
3. Set development environment variables by copy and paste `env.example` file then rename it to `.env`
4. Edit file `.env` and fill the value ( If you don't know about the value, please ask to other teammates)
5. Run `bundle install`
6. Initiate database (see below)
7. Start the server by `sh start.sh`
8. Access from port 8080

# Database Initialization
```
rake db:create
rake db:migrate
```

# What's Left
unit test
CRUD on table with relation
